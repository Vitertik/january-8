﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace session2_console
{
    class Program
    {
        public delegate void MyDelegate(int param1, string param2);

        public delegate void ReportingDelegate(string msg);

        static void ReportIntoAFile(string message) {
            File.AppendAllText(@"..\..\log.txt", message);
        }

        static void ReportOnScreen(string message)
        {
            Console.WriteLine(message);
        }

        static string[] nameList = new string[5];
        static void Main(string[] args)
        {
            //I have differenet methods with diff names and implementaions
            //and based of some conditions instead of calling the methods each time
            //I would like to use delegate to invoke those methods

            //delegate
            MyDelegate del = Print;

            del.Invoke(18, "Adam");

            del = PrintHappy;
            //if (del != null)
            //{
            //    del.Invoke(5, "Adam");
            //}
            del?.Invoke(5, "Adam");

            //Console.ReadKey();

            //Create a list of names
            //and then ask user to enter a name
            //if the name is found/not found
            //randomly I logged the result either on screen or in a file
            //logging a message (string message) and return void 

            ReportingDelegate reportingDelegate = null;

            Random random = new Random();
            if (random.Next(0,2) == 1)
            {
                reportingDelegate += ReportOnScreen;
            }

            if (random.Next(0, 2) == 1)
            {
                reportingDelegate += ReportIntoAFile;
            }


            for (int i=0; i< nameList.Length; i++)
            {
                Console.WriteLine("enter a name");
                nameList[i] = Console.ReadLine();
            }

            Console.WriteLine("Enter a name to Search");
            string name = Console.ReadLine();

            foreach(string n in nameList)
            {
                if (n.Equals(name))
                {
                    reportingDelegate?.Invoke($"{name} is found");
                }
            }

            Console.ReadKey();
        }


        public static void Print(int age, string name)
        {
            Console.WriteLine($"{name} can drink after {age}");
        }


        public static void PrintHappy(int age, string name)
        {
            Console.WriteLine($"{name} goes to school after {age}");

        }
    }



    //List<Person> people = new List<Person>();
    //people.Add(new Student());
    //people.Add(new Student());
    //people.Add(new Teacher());
    //people.Add(new Teacher());

    ////Singleton
    ////MVC
    ////Factory

    //foreach (var p in people)
    //{
    //    if(p is Teacher)
    //    {
    //        Teacher teacher = (Teacher)p;
    //        teacher.RegisterTeacher();
    //    }
    //    if(p is Student)
    //    {
    //        Student std = (Student)p;
    //        std.RegisterStudent();
    //    }
    //}
    //Console.ReadKey();
}
